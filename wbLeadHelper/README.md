A simple warhammer online addon which makes it easier for warband and siege leaders to communicate with their followers by broadcasting text sequences into chat. 

Functionality:

	* type /wlh to open the addon
	
	* Leftclick on a message label to broadcast the message to the warband (/wb) channel
	* Rightclick on a message label to broadcast the message to the region (/1) channel

	* Click on a countdown label to start a countdown with the noted time, click it again to abort the countdown.

	* Click on a "ZONE" message label to dynamically select a zone. $Z in a message text will be replaced with the selected zone.


Configuration:

	* Right now there is no way to change the configuration in game you have to edit the file wbLeadHelper.lua

	* It's easy, at the top of the file you can find this to change the settings:

~~~~
	local textColor = "255,54,54"; -- You can get the numbers for the colors here: https://www.rapidtables.com/web/color/RGB_Color.html
	local windowSize = 1; -- scales the size of the window (doh..) 0.5 is 50% for example
	local wlhMessages = {
		-- First value is the message label which is shown in the overview. Second is the message that is put in chat.
		"Countdown 5 | This is a countdown of 5 seconds",
		"Countdown 30 | This is a countdown of 30 seconds",
		"Ready to move | Get ready to move, GATHER at my position",
		"Moving, follow | MOVING, follow me",
		"Stay | STAY at my position",
		"Hold, no attack | Hold position, DON'T attack",
		"Attack | Attack in my direction",
		"Enemy Behind | ENEMY BEHIND",
		"Release | RELEASE, we are beaten",
		"kick explain | This is an organized warband, please understand that you will be kicked in 1 minute if you are not close to me by then. Thanks",
		"OIL | OIL OIL OIL",
		"Get R to push | GET READY TO PUSH",
		"Relocate to Zone! | RELOCATION! Go to zone * $Z *"
	}
~~~~

----------------

Installation:

	* Download the addon here: https://gitlab.com/sxe/RoRAddonFixes/-/archive/master/RoRAddonFixes-master.zip?path=wbLeadHelper

    * copy/extract the folder wbLeadHelper into your "Warhammer Online - Age of Reckoning/Interface/AddOns/" folder.

-----------------
0.7 - Updated as of 7/22/2020.
* Added "Color Mode", to be able to chat colored all the time.

0.6.5 - Updated as of 7/16/2020.
* Button text can now be colored
* Added "back" button to submenus
* Realigned text in buttons to better fit
* Submenus are now better recognizable

0.6 - Updated as of 7/13/2020.
* Added button to shout out a BO 
* Added a "Looking for Members" button which post to the /5 LFG channel on left click. Or to /1 on right click.
* Some bug fixes and code optimizations

0.5 - Updated as of 6/24/2020.
* Reworked countdown functionality

0.4 - Updated as of 6/24/2020.
* Improved window scaling
* Added more messages
* Added a new layout (two button rows) 
* Added mouse over button color change 

0.3 - Updated as of 6/06/2020.
* Added a Zone announcement message

0.2.3 - Updated as of 6/04/2020.
* Added a variable at the top of the document to make it possible to change the size of the window

0.2.2 - Updated as of 6/03/2020.
* Added a variable at the top of the document to make changing the text color easier

0.2 - Updated as of 6/02/2020.
* Initial Release