if not wbLeadHelper then wbLeadHelper = {} end


---
-- Add your own messages here.
---
local wlhMessageStart = "<icon49>"; -- Will be added to the start of the message. Don't use >> it won't work!
local wlhMessageEnd = "<icon49>"; -- Will be added to the end of the message.
local textColor = "255,54,54"; -- You can get the numbers for the colors here: https://www.rapidtables.com/web/color/RGB_Color.html
local windowSize = 0.65; -- scales the size of the window (doh..) 0.5 is 50% for example
local wlhMessages = {
	-- First value is the message label which is shown in the overview. Second is the message that is put in chat.
	"*Count\n down* | $C This is the countdown menu, don't rename it!",
	"Check buffs | Check your buffs, guard and potions",
	"<green> Ready to move | Get ready to move, GATHER at my position",
	"<green> Moving, follow | MOVING, follow me",
	"<yellow> Stay | STAY at my position",
	"<yellow> Hold, no attack | Hold position, DON'T attack",
	"<yellow> Get R\n to push | GET READY TO PUSH",
	"<red> Attack | Attack in my direction",
	"<red> Enemy Behind | ENEMY BEHIND",
	"<red> OIL | OIL OIL OIL",
	"<blue> Regroup | Regroup at my position",
	"<blue> Rel & Reg | RELEASE and regroup on me",
	"<blue> kick\nexplain | This is an organized warband, please understand that you will be kicked in 1 minute if you are not close to me by then. Thanks",
	"<green> *Move\n to BO* | MOVING! Go to BO - $B",
	"<green> *Relocate\n to Zone* | RELOCATION! Go to zone - $Z",
	"*LFM* | Organized warband looking for: $LFM. Please message me"

}

-- template 1
local titleWidth = 254
local buttonWidth = 126;
local buttonHeight = 54;
local buttonsPerRow = 8;

-- template 2
--local titleWidth = 220
--local buttonWidth = 220;
--local buttonHeight = 34;
--local buttonsPerRow = 14;




local wbColors = {
	white = {255, 255, 255},
    red = {240, 60, 60},
    green = {135, 230, 170},
    blue = {60, 120, 200},
    yellow = {255, 170, 70},
    grey = {150, 150, 150},
    black = {0, 0, 0}    
}

--
--
--
--
-- DON'T CHANGE ANYTHING BELOW THIS LINE 
--
--
--
--

local towstring = towstring
local CreateHyperLink = CreateHyperLink
local DoesWindowExist = DoesWindowExist
local table_insert = table.insert;
local table_sort = table.sort;
local wbLeadHelperWindowName = "wbLeadHelperWindow";
local showingAlternateTitle = false;
local inLoadingScreen = false;
local timerActive = 0;
local activeChatChannel = "";
local zoneMenuActive = false;
local boMenuActive = false;
local lfmMenuActive = false;
local countdownMenuActive = false;
local tmpMessageActive = "";
local hookOnKeyEnter;
local chatHookActive = false;

local function Print(str)
	EA_ChatWindow.Print(towstring(str));
end

function wbLeadHelper.OnInitialize()

	hookOnKeyEnter = EA_ChatWindow.OnKeyEnter;
	EA_ChatWindow.OnKeyEnter = wbLeadHelper.OnKeyEnter;
	chatHookActive = false

	if (DoesWindowExist(wbLeadHelperWindowName) == false) then
		wbLeadHelper.createWbLeadHelperWindow();
	end
	
	RegisterEventHandler( SystemData.Events.LOADING_BEGIN, "wbLeadHelper.LOADING_START");
	RegisterEventHandler( SystemData.Events.LOADING_END, "wbLeadHelper.LOADING_END");
	
	if LibSlash then
		LibSlash.RegisterSlashCmd("wlh", function(args) wbLeadHelper.SlashCmd(args) end);
		Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"50,255,10\"> Addon initialized. Use /wlh to toggle.");
	else
		Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"50,255,10\"> Addon initialized.");
	end
	
end

function wbLeadHelper.OnShutdown()
	EA_ChatWindow.OnKeyEnter = hookOnKeyEnter;
	chatHookActive = false
end

function wbLeadHelper.LOADING_START()
	inLoadingScreen = true;
end

function wbLeadHelper.LOADING_END()
	inLoadingScreen = false;
end


function wbLeadHelper.SlashCmd(args)

	local command;
	local parameter;
	local separator = string.find(args," ");
	
	if separator then
		command = string.sub(args, 0, separator - 1);
		parameter = string.sub(args, separator + 1, -1);
	else
		command = args;
	end
	
	if command == "" then wbLeadHelper.toggleWbLeadHelperWindow();
	else Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"255,50,50\"> Unknown command.");
	end
	
end

function wbLeadHelper.createWbLeadHelperWindow()

	if (DoesWindowExist(wbLeadHelperWindowName) == true) then return end

	CreateWindow(wbLeadHelperWindowName, false);
	showingAlternateTitle = false;
	
	-- window background
	WindowSetTintColor(wbLeadHelperWindowName .. "Background", 255, 0, 0);
	WindowSetAlpha(wbLeadHelperWindowName .. "Background", 0);
	WindowSetDimensions( wbLeadHelperWindowName .. "Background", 250, 40);
	
	-- title label background
	WindowSetTintColor(wbLeadHelperWindowName .. "TitleBackground", 0, 0, 0);
	WindowSetAlpha(wbLeadHelperWindowName .. "TitleBackground", 0.65);

	WindowSetDimensions( wbLeadHelperWindowName .. "Title", titleWidth, 30);
	WindowSetDimensions( wbLeadHelperWindowName .. "TitleLabel", titleWidth, 30);

	ButtonSetText (wbLeadHelperWindowName .."CloseButton", L"X");
	ButtonSetText (wbLeadHelperWindowName .."ChatButton", L"CM");

	WindowSetScale( wbLeadHelperWindowName, windowSize );

	wbLeadHelper.draw();
	
end

local TIME_DELAY2 = 1;
local timeLeft2 = 1;
local timerDelay = 5;
function wbLeadHelper.timerUpdate(elapsed)

	if (inLoadingScreen == true) then return end

	timeLeft2 = timeLeft2 - elapsed;
    if (timeLeft2 > 0) then
        return;
    end
	
	if (timerActive > 10) then
		if (timerDelay == 1 ) then
			--d(timerActive);
			--d(activeChatChannel);
			wbLeadHelper.chat(timerActive, activeChatChannel);
			timerDelay = 5;
		else
			timerDelay = timerDelay -1;
		end
		--d("delay: " .. tostring(timerDelay));
	elseif (timerActive > 0) then
		--d(timerActive);
		--d(activeChatChannel);
		wbLeadHelper.chat(timerActive, activeChatChannel);
		timerDelay = 5;
	end
	if (timerActive > 0) then
		timerActive = timerActive -1;
	end
	

	timeLeft2 = TIME_DELAY2;
	
end

function wbLeadHelper.draw(toggle)

	if ( zoneMenuActive == true) then return end
	if ( boMenuActive == true) then return end
	if ( lfmMenuActive == true) then return end
	if ( countdownMenuActive == true) then return end
	
	wbLeadHelper.drawWindows(wlhMessages, toggle);

end

function wbLeadHelper.drawCountdownMenu(countdowns)
	wbLeadHelper.drawWindows(countdowns);
end

function wbLeadHelper.drawZoneMenu(zoneNames)
	wbLeadHelper.drawWindows(zoneNames);
end

function wbLeadHelper.drawBoMenu(bos)
	wbLeadHelper.drawWindows(bos);
end

function wbLeadHelper.drawLfmMenu(lfm)
	wbLeadHelper.drawWindows(lfm);
end

function wbLeadHelper.drawWindows(items, toggle)

	if ( not items ) then return end

	local isVisible = WindowGetShowing(wbLeadHelperWindowName);
	if (toggle) then
		WindowSetShowing(wbLeadHelperWindowName, not isVisible);
	end

	wbLeadHelper.destroyWindows(100);

	-- draw windows for all messages
	local xOffset = 0;
	local yOffset = (3 * windowSize);
	local bottomWindow = "wbLeadHelperWindowTitle";
	for i=1, #items do

		local name = tostring(items[i]);
		name =  wbLeadHelper.splitMessage(name,"|");
		name = wbLeadHelper.trim(name[1]);

		local color = wbColors["white"];
		if(name:match("<%w+>")) then
			color = name:match("%w+");
			color = wbColors[tostring(color)];
			name = name:gsub( "<%w+>", "");
			name = wbLeadHelper.trim(name);
		end
	
		local messageWindow = "wbLeadHelper_Message_" .. tostring(i);	
		if (DoesWindowExist(messageWindow) == false) then
			CreateWindowFromTemplate( messageWindow, "wbLeadHelper_Message_Template", "Root");
		end
		
		WindowSetShowing( messageWindow, isVisible);
		WindowSetTintColor(messageWindow .. "Background", 0, 0, 0);
		WindowSetAlpha(messageWindow .. "Background", 0.6);

		LabelSetText( messageWindow .. "Label", towstring(name));
		LabelSetTextColor(messageWindow .. "Label", color[1], color[2], color[3])

		WindowSetDimensions( messageWindow .. "Label", buttonWidth, buttonHeight);
		WindowClearAnchors( messageWindow );
		WindowSetDimensions( messageWindow, buttonWidth, buttonHeight);

		WindowAddAnchor( messageWindow , "bottomleft", bottomWindow, "topleft", (xOffset / InterfaceCore.GetScale()) * windowSize, (2 / InterfaceCore.GetScale()) * windowSize );
		WindowSetScale( messageWindow, windowSize );
				
		if (toggle) then
			WindowSetShowing( messageWindow, not isVisible);
		else
			WindowSetShowing( messageWindow, isVisible);
		end

		bottomWindow = messageWindow;

		if (i % buttonsPerRow == 0) then
			xOffset = xOffset + (buttonWidth + 2) * (i / buttonsPerRow);
			bottomWindow = "wbLeadHelperWindowTitle";
		else 
			xOffset = 0;
		end

	end

	if (showingAlternateTitle == false) then
		wbLeadHelper.showNormalTitle();
	end
end

function wbLeadHelper.destroyWindows(number)

	if ( not number ) then return end

	--remove existing message windows
	for i=1, number do
	
		local messageWindow = "wbLeadHelper_Message_" .. tostring(i);	
		if (DoesWindowExist(messageWindow) == true) then
			DestroyWindow(messageWindow);
		end
	end	
end

function wbLeadHelper.toggleWbLeadHelperWindow()
	wbLeadHelper.draw(true);	
end

function wbLeadHelper.hideWbLeadHelperWindowName()
	wbLeadHelper.toggleWbLeadHelperWindow();
end

function wbLeadHelper.onLMB()
	wbLeadHelper.toChat("lmb");
end

function wbLeadHelper.onRMB()
	wbLeadHelper.toChat("rmb");
end

function wbLeadHelper.toChat(mouseButton)

	local selectedWindow = towstring(SystemData.ActiveWindow.name);
	local mouseoverMessageId = selectedWindow:match(L"wbLeadHelper_Message_(%d+)");
	mouseoverMessageId = tonumber(mouseoverMessageId);

	local message = "";
	if ( zoneMenuActive ) then
		local zoneNames = wbLeadHelper.getAllZoneNames();
		message = towstring(zoneNames[mouseoverMessageId]);
	elseif ( boMenuActive ) then
		local bos = wbLeadHelper.getBObyZoneId(wbLeadHelper.getCurrentZone());
		message = towstring(bos[mouseoverMessageId]);
	elseif ( lfmMenuActive ) then
		local lfm = wbLeadHelper.getLfgNames();
		message = towstring(lfm[mouseoverMessageId]);
	elseif ( countdownMenuActive ) then
		local countdowns = wbLeadHelper.getCountDowns();
		message = towstring(countdowns[mouseoverMessageId]);
	else
		message = tostring(wlhMessages[mouseoverMessageId]);
		message =  wbLeadHelper.splitMessage(message,"|");
		message = towstring(wbLeadHelper.trim(message[2]));
	end

	local chat = wbLeadHelper.setChatChannel(mouseButton, message);

	if(countdownMenuActive) then
		-- countdown menu is active
		wbLeadHelper.destroyWindows(100);

		if(message:match(L"<- BACK -")) then
		else
			timerVal = tonumber(message);
			timerActive = timerVal -1; -- this value is monitored by the game update function.
			activeChatChannel = chat;
			wbLeadHelper.chat( timerVal .. " sec countdown started!", chat); --only the initial value is put in chat here
		end
		countdownMenuActive = false;
		wbLeadHelper.draw(); -- draw here just to avoid a delay in the messages showing up again

	-- zoneMenu is active 
	elseif (zoneMenuActive) then

		wbLeadHelper.destroyWindows(100);

		if(message:match(L"<- BACK -")) then
		else
			local newMessage = message:upper();
			newMessage = tmpMessageActive:gsub( L"$Z", newMessage);
			wbLeadHelper.chat(towstring(newMessage), chat);
		end
		zoneMenuActive = false;
		tmpMessageActive = "";

		wbLeadHelper.draw(); -- draw here just to avoid a delay in the messages showing up again

	-- boMenu is active 
	elseif (boMenuActive) then

		wbLeadHelper.destroyWindows(100);

		if(message:match(L"<- BACK -")) then
		else
			local newMessage = message:upper();
			newMessage = tmpMessageActive:gsub( L"$B", newMessage);
			wbLeadHelper.chat(towstring(newMessage), chat);
		end
		boMenuActive = false;
		tmpMessageActive = "";

		wbLeadHelper.draw(); -- draw here just to avoid a delay in the messages showing up again

	elseif (lfmMenuActive) then

		wbLeadHelper.destroyWindows(100);

		if(message:match(L"<- BACK -")) then
		else
			local newMessage = message:upper();
			newMessage = newMessage:gsub( L"/%d+", "");
			newMessage = tmpMessageActive:gsub( L"$LFM", newMessage);
			wbLeadHelper.chat(towstring(newMessage), chat);
		end
		lfmMenuActive = false;
		tmpMessageActive = "";

		wbLeadHelper.draw(); -- draw here just to avoid a delay in the messages showing up again

	-- check if countdown menu was clicked
	elseif (message:match(L"$C")) then

		if(timerActive ~= 0) then
			timerActive = 0;
			wbLeadHelper.chat(L"Countdown aborted!", chat);
		else
			countdownMenuActive = true;
			local countdowns = wbLeadHelper.getCountDowns();
			wbLeadHelper.drawCountdownMenu(countdowns);			
		end

	-- check if message contains a zone variable ($Z)
	elseif (message:match(L"$Z")) then

		zoneMenuActive = true;
		tmpMessageActive = message;

		local zoneNames = wbLeadHelper.getAllZoneNames();

		wbLeadHelper.drawZoneMenu(zoneNames);
	-- check if message contains a lfm variable ($LFM)
	elseif (message:match(L"$LFM")) then

		lfmMenuActive = true;
		tmpMessageActive = message;

		local lfmNames = wbLeadHelper.getLfgNames();

		wbLeadHelper.drawLfmMenu(lfmNames);

	-- check if message contains a bo variable ($B)
	elseif (message:match(L"$B")) then

		local bos = wbLeadHelper.getBObyZoneId(wbLeadHelper.getCurrentZone());

		boMenuActive = true;
		tmpMessageActive = message;

		wbLeadHelper.drawBoMenu(bos);

	else
		wbLeadHelper.chat(message, chat);		
	end
end

function wbLeadHelper.chat(message, channel)
	local message = channel .. towstring(wlhMessageStart) .. L" <LINK data=\"0\" text=\"" .. towstring(message) .. L"\" color=\"" .. towstring(textColor) .. L"\"> " .. towstring(wlhMessageEnd);

	--EA_ChatWindow.InsertText(message);
	SendChatText( message, L"" );	
	--d(message)
end

function wbLeadHelper.setChatChannel(mb, text)

	local chat = L"/1 ";

	if (mb == "lmb") then
		chat = L"/wb ";

		if (text) then
			if(text:match(L"/%d ")) then
				chat = text:match(L"/%d ");
			end
		end
	end
	return chat;
end


function wbLeadHelper.onMouseOver()
	wbLeadHelper.onZoneMouseOver();

	local hoverWindow = tostring(SystemData.MouseOverWindow.name);
	local color = wbColors["grey"];
	WindowSetTintColor(hoverWindow, color[1], color[2], color[3]);
end

function wbLeadHelper.onMouseOut()
	wbLeadHelper.showNormalTitle();

	local hoverWindow = tostring(SystemData.MouseOverWindow.name);
	local color = wbColors["black"];	
	WindowSetTintColor(hoverWindow, color[1], color[2], color[3]);		
end

function wbLeadHelper.onZoneMouseOver()
	LabelSetText("wbLeadHelperWindowTitleLabel", L"wb <icon00092> / <icon00093> region");

	showingAlternateTitle = true;			
end

function wbLeadHelper.showNormalTitle()
	LabelSetText("wbLeadHelperWindowTitleLabel", L"wbLeadHelper");
end

function wbLeadHelper.splitMessage(s, delimiter)
    result = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match);
    end
    return result;
end

function wbLeadHelper.trim(s)
   return (s:gsub("^%s*(.-)%s*$", "%1"))
end

function wbLeadHelper.getAllZoneNames()
	local zones = {
		--Dwarfs vs Greenskins
		--6, -- L"Ekrund",
		--11, -- L"Mount Bloodhorn",
		7, -- L"Barak Varr",
		1, -- L"Marshes of Madness",
		2, -- L"The Badlands",
		8, -- L"Black Fire Pass",
		9, -- L"Kadrin Valley",
		5, -- L"Thunder Mountain",
		3, -- L"Black Crag",

		-- Forts
		4, -- L"Butcher's Pass",
		10, -- L"Stonewatch",

		--Chaos vs Empire
		--100, -- L"Norsca",
		--106, -- L"Nordland",
		107, -- L"Ostland",
		101, -- L"Troll Country",
		102, -- L"High Pass",
		108, -- L"Talabecland",
		103, -- L"Chaos Wastes",
		105, -- L"Praag",
		109, -- L"Reikland",

		-- Forts
		104, -- L"The Maw",
		110, -- L"Reikwald",

		--Cities
		--161, -- L"The Inevitable City",
		--162, -- L"Altdorf",

		--Land of the Dead
		--191, -- L"Necropolis of Zandri",

		--High Elves vs Dark Elves
		--200, -- L"The Blighted Isle",
		--206, -- L"Chrace",
		201, -- L"The Shadowlands",
		207, -- L"Ellyrion",
		202, -- L"Avelorn",
		208, -- L"Saphery",
		203, -- L"Caledor",
		205, -- L"Dragonwake",
		209, -- L"Eataine"

		-- Forts
		204, -- L"Fell Landing"
		210 -- L"Shining Way"
	};

	local zoneNames = {};
	table_insert(zoneNames, "<- BACK -");

	for i=1, #zones do
		local zoneName = GetZoneName(zones[i]);

		--d(zoneName .. L" " .. zoneNAme2);

		table_insert(zoneNames, zoneName);
	end
	return zoneNames;
end

function wbLeadHelper.getCountDowns()
	local countdowns = {
		5,
		10,
		20,
		30,
		60,
		120,
		180
	};
	table_insert(countdowns, 1, "<- BACK -");
	return countdowns;
end

function wbLeadHelper.getCurrentZone()
	return GameData.Player.zone;
end

function wbLeadHelper.getBObyZoneId(id)
	local bos = {
		[6] = {"Cannon Battery", "Stonemine Tower"}, -- L"Ekrund",
		[11] = {"Ironmane Outpost", "The Lookout"}, -- L"Mount Bloodhorn",
		[7] = {"The Lighthouse", "The Ironclad"}, -- L"Barak Varr"
		[1] = {"Alcadizaar's Tomb", "Goblin Armory"}, -- L"Marshes of Madness",
		[2] = {"Goblin Artillery Range", "Karagaz"}, -- L"The Badlands",
		[8] = {"Bugman's Brewery", "Furrig's Fall"}, -- L"Black Fire Pass",
		[9] = {"Dolgrund's Cairn", "Gromril Junction", "Hardwater Falls", "Icehearth Crossing"}, -- L"Kadrin Valley",
		[5] = {"Doomstriker Vein", "Gromril Kruk", "Karak Palik", "Thargrim's Headwall"}, -- L"Thunder Mountain",
		[3] = {"Lobba Mill", "Madcap Pickins", "Rottenpike Ravine", "Squiggly Beast Pens"}, -- L"Black Crag",
		[100] = {"Lost Lagoon"}, -- L"Norsca",
		[106] = {"Festenplatz", "The Harvest Shrine", "The Nordland XI"}, -- L"Nordland",		
		[107] = {"Crypt of Weapons", "Kinschel's Stronghold"}, -- L"Ostland",
		[101] = {"Monastery of Morr", "Ruins of Greystone Keep"}, -- L"Troll Country",
		[102] = {"Feiten's Lock", "Hallenfurt Manor", "Ogrund's Tavern"}, -- L"High Pass",
		[108] = {"Verentane's Tower"}, -- L"Talabecland",
		[103] = {"Chokethorn Bramble", "Thaugamond Massif", "The Shrine of Time", "The Statue of Everchosen"}, -- L"Chaos Wastes",
		[105] = {"Kurlov's Armory", "Manor of Ortel von Zaris", "Martyr's Square", "Russenscheller Graveyard"}, -- L"Praag",
		[109] = {"Frostbeard's Quarry", "Reikwatch", "Runehammer Gunworks", "Schwenderhalle Manor"}, -- L"Reikland",
		[200] = {"The Altar of Khaine", "The House of Lorendyth"}, -- L"The Blighted Isle",
		[206] = {"The Shard of Grief", "The Tower of Nightflame"}, -- L"Chrace",		
		[201] = {"The Shadow Spire", "The Unicorn Siege Camp"}, -- L"The Shadowlands",
		[207] = {"The Needle of Ellyrion", "The Reaver Stables"}, -- L"Ellyrion",
		[202] = {"Maiden's Landing", "The Wood Choppaz"}, -- L"Avelorn",
		[208] = {"The Spire of Teclis", "Sari' Daroir"}, -- L"Saphery",
		[203] = {"Druchii Barracks", "Sarathanan Vale", "Senlathain Stand", "Shrine of the Conqueror"}, -- L"Caledor",
		[205] = {"Fireguard Spire", "Milaith's Memory", "Mournfire's Approach", "Pelgorath's Ember"}, -- L"Dragonwake",
		[209] = {"Bel-Korhadris' Solitude", "Chillwind Manor", "Sanctuary of Dreams", "Ulthorin Siege Camp"}, -- L"Eataine"
		[4] = {"Northwestern BO", "Northeastern BO", "Southeastern BO", "Southern BO", "Southwestern BO"}, -- L"Butcher's Pass",
		[10] = {"Northern BO", "Eastern BO", "Southeastern BO", "Southwestern BO", "Western BO"}, -- L"Stonewatch",
		[104] = {"Eastern BO", "Southeastern BO", "Southern BO", "Southwestern BO", "Northwestern BO"}, -- L"The Maw",
		[110] = {"Northeastern BO", "Southeastern BO", "Southern BO", "Southwestern BO", "Northwestern BO"}, -- L"Reikwald",
		[204] = {"Northern BO", "Northeastern BO", "Southeastern BO", "Southwestern BO", "Northwestern BO"}, -- L"Fell Landing"
		[210] = {"Northern BO", "Eastern BO", "Southern BO", "Southwestern BO", "Northwestern BO"}, -- L"Shining Way"		
		[161] = {"Emissary of the Changer", "Khorne War Quaters", "Slaanesh Chambers", "Temple of the Damned", "The Lyceum", "The Outcrop"}, -- L"The Inevitable City",
		[162] = {"Emperor's Circle", "The Armory", "The Library", "The North Dock", "The South Dock", "The Siege Workshop"}, -- L"Altdorf",		
	};

	local posterns = {
		"Destro Keep",
		"Order Keep",
		"North Postern",
		"East Postern",
		"South Postern",
		"West Postern"
	};

	if(bos[id]) then
		bos = bos[id];
		for _, v in pairs(posterns) do
    		table.insert(bos, v)
		end
	else
		bos = posterns;
	end

	table_insert(bos, 1, "<- BACK -");
	
	return bos;
end

function wbLeadHelper.getLfgNames()
	local names = {
		"/5 HEALER / TANK / DPS",
		"/5 HEALER / TANK",
		"/5 HEALER",
		"/5 TANK",
		"/5 DPS"
	};
	table_insert(names, 1, "<- BACK -");
	return names;
end

function wbLeadHelper.toggleColoredChat()
	if not chatHookActive then
		Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"50,255,10\"> Permanent colored chat enabled.");
		chatHookActive = true;
	else
		Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"50,255,10\"> Permanent colored chat disabled.");
		chatHookActive = false;
	end
end

function wbLeadHelper.OnKeyEnter(...)
	if not chatHookActive then
		return hookOnKeyEnter(...)
	end

	local textInput = wbLeadHelper.trim(tostring(EA_TextEntryGroupEntryBoxTextInput.Text));

	if (textInput == "" or textInput == nil or textInput:sub(1,1) == "/" or textInput:sub(1,1) == "." or textInput:sub(1,1) == "]" or textInput:sub(1,1) == "*" ) then
		return hookOnKeyEnter(...);
	end
	
	textInput = wlhMessageStart .. " <LINK data=\"0\" text=\"" .. textInput .. "\" color=\"" .. textColor .. "\"> " .. wlhMessageEnd;

	EA_TextEntryGroupEntryBoxTextInput.Text = towstring(textInput);

	return hookOnKeyEnter(...);
end