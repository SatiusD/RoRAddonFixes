<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="wbLeadHelper" version="0.7" date="07/20/2020" >
		<Author name="Norib" />
		<Description text="Helps warband leaders to communicate with their warband members. Run /wlh command to bring up the window." />
		<VersionSettings gameVersion="1.4.8" windowsVersion="0.1" />
		<Dependencies>
			<Dependency name="LibSlash" optional="false" forceEnable="true" />
		</Dependencies>

		<Files>
            <File name="wbLeadHelper.lua" />
            <File name="wbLeadHelper.xml" />
        </Files>
		
		<OnInitialize>
			<CallFunction name="wbLeadHelper.OnInitialize" />
		</OnInitialize>
		
		<OnUpdate>
			<CallFunction name="wbLeadHelper.timerUpdate"/>
		</OnUpdate>
		
		<OnShutdown>
			<CallFunction name="wbLeadHelper.OnShutdown" />
		</OnShutdown>

		<WARInfo>    
		  <Categories>
			<Category name="RVR" />
		  </Categories>
		</WARInfo>
	</UiMod>
</ModuleFile>